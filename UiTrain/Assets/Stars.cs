using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Stars : MonoBehaviour
{
    private Transform star1;
    private Transform star2;
    private Transform star3;
    private int rnd;

    void Start()
    {
        star1 = transform.GetChild(4);
        star2 = transform.GetChild(5);
        star3 = transform.GetChild(6);
        rnd = Random.Range(1, 4);
    }

    void Update()
    {
        if(rnd == 1)
        {
            star1.gameObject.SetActive(true);
        } else if (rnd == 2)
        {
            star1.gameObject.SetActive(true);
            star2.gameObject.SetActive(true);
        } else if (rnd == 3)
        {
            star1.gameObject.SetActive(true);
            star2.gameObject.SetActive(true);
            star3.gameObject.SetActive(true);
        }
    }
}
