using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class AddLvl : MonoBehaviour
{
    [Range(5,21)] [SerializeField] private int lvlCount;
    [SerializeField] private GameObject lvlBtn;

    void Start()
    {
        for (int i = 0; i < lvlCount; i++)
        {
            GameObject addedObj = Instantiate(lvlBtn, transform);
            addedObj.name = "Button" + $"{i}";
            addedObj.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = $"{i}";
        }
    }
}
